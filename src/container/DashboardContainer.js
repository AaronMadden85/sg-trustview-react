import React, { Component } from 'react';

import '../css/App.css';
import NavHeaderComponent from '../components/NavHeaderComponent';
import NavLeftColComponent from '../components/NavLeftColComponent';
import NavRightColComponent from '../components/NavRightColComponent';
import NavRightColComponentDummy from '../components/NavRightColComponentDummy';
import NavLeftColComponentDummy from '../components/NavLeftColComponentDummy';

class DashboardContainer extends Component {
  constructor() {
    super()
    this.state = {
      listings: [],
      mode: 'logged_out'
    }

    this.handleAuth = this.handleAuth.bind(this);

  }
  
handleAuth() {
  this.setState({ mode: 'logged_in' });
}

renderContent() {
  if(this.state.mode !== 'logged_in') {
    return(
      <div>
        <NavLeftColComponent propsData={this.state}></NavLeftColComponent>
        <NavRightColComponent propsData={this.state}></NavRightColComponent>
      </div>
    )
  } else {
    return(
      <div>
        <NavLeftColComponentDummy propsData={this.state}></NavLeftColComponentDummy>
        <NavRightColComponentDummy propsData={this.state}></NavRightColComponentDummy>
      </div>
    )
  }
}

render() {
    return (
        <div className="outer-container">
            <NavHeaderComponent onAuth={this.handleAuth}></NavHeaderComponent>
            <div className="contentsContainer">
              {this.renderContent()}
            </div>
        </div>
    );
  }
}
export default DashboardContainer;