import React, { Component } from 'react';

import './css/App.css';

import DashboardContainer from './container/DashboardContainer'

class App extends Component {
  render() {
    return (
        <DashboardContainer />
    );
  }
}

export default App;