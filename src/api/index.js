export function getListings(baseUrl) {
  console.log('0. calling getListings...')
  return fetch(`${baseUrl}`)
  .then( console.log('step 1, returning response.json()...') )
  .then( response => response.json() )
}