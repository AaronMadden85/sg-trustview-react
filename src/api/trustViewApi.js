import axios from 'axios';

const webApiBaseUri = 'https://4c760ol4kb.execute-api.eu-west-1.amazonaws.com/dev/'

class TrustViewApi {
	
	getAllSectors() {
		return axios.get(`${webApiBaseUri}sectors`);
	}

	getClientsBySector(setorId) {
		return axios.get(`${webApiBaseUri}clients?sectorid=${setorId}`);
	}

	getEtls(brandid, date) {
		return axios.get(`${webApiBaseUri}etl?brandid=${brandid}&date=${date}`);
	}

	getTopics(brandid, date) {
		return axios.get(`${webApiBaseUri}topics?brandid=${brandid}&date=${date}`);
	}

	getWeeklyPressuresBySector(sectorId, date) {
		return axios.get(`${webApiBaseUri}weeklypressures?sectorid=${sectorId}&date=${date}`);
	}

	getWeeklyPressuresByBrand(brandid, startdate, enddate) {
		return axios.get(`${webApiBaseUri}weeklypressuresbybrandid?brandid=${brandid}&startdate=${startdate}&enddate=${enddate}`);
	}
}

export default TrustViewApi;