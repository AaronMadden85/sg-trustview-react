

import React, { Component } from 'react';
//import * as api from '../api'

class HeaderLoginSwitchComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mode:'logged_in',
            sector:null,
            client:null,
            uservalue:'',
            passvalue:'',
            apiData:''
        }

        this.handleLogIn = this.handleLogIn.bind(this);
        this.handleSectorChange = this.handleSectorChange.bind(this);
        this.handleClientChange = this.handleClientChange.bind(this);
        this.handleChangeUser = this.handleChangeUser.bind(this);
        this.handleChangePass = this.handleChangePass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.dropDownValidation = this.dropDownValidation.bind(this);
        this.getApi = this.getApi.bind(this);
    }

    componentDidMount() {
        //this.getApi('https://4c760ol4kb.execute-api.eu-west-1.amazonaws.com/dev/sectors', 'logged_out');
    }

    getApi(url, state) {
        fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ apiData:JSON.parse(data), mode:state }));
    }

    handleLogIn() {
        this.setState({ mode: 'log_in' });
    }
    handleChangeUser(e) {
        this.setState({uservalue: e.target.value});
    }
    handleChangePass(e) {
        this.setState({passvalue: e.target.value});
    }
    handleSubmit(e) {
        if(this.state.uservalue !== '' && this.state.passvalue !== '') {
            this.setState({ mode: 'select_id' });
        } else {
            alert('Please enter a username and password.');
        }
    }
    handleSectorChange(e) {
        this.setState({ sector: e.target.value });
    }
    handleClientChange(e) {
        this.setState({ client: e.target.value });
    }
    dropDownValidation(e) {
        if(this.state.sector !== null && this.state.client !== null) {
            this.props.onAuth();
            this.setState({ mode: 'logged_in' });
        }
    }

    renderLoginField(e) {
        if(this.state.mode === 'logged_out') {
            return(
                <button onClick={this.handleLogIn}>
                        Login
                </button>
            )
        } else if(this.state.mode === 'log_in') {
            return(
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <input placeholder="Username" type="text" value={this.state.uservalue} onChange={this.handleChangeUser} />
                        <input placeholder="Password" type="text" value={this.state.passvalue} onChange={this.handleChangePass} />
                        <input type="submit" value="Login" />
                    </form>
                </div>
            )
        } else if(this.state.mode === 'select_id') {
            console.log(this.state.apiData);
            return(
                <div>
                    <select onChange={this.handleSectorChange}>
                        <option value="0">Select sector:</option>
                        <option value="1">Oil</option>
                    </select> 
                    <select onChange={this.handleClientChange}>
                        <option value="0">Select client:</option>
                        <option value="1">BP</option>
                    </select>
                    <input type="image" onClick={this.dropDownValidation} src="./img/mag.png" alt="Submit" width="30" height="30"></input>
                </div>
            )
        } else if(this.state.mode === 'logged_in') {
            return(
                <div>
                    <input type="image" onClick={this.handleSubmit} src="./img/mag.png" alt="Submit" width="30" height="30"></input>
                </div>
            )
        }
    }

    render () {
        return (
          <div>
            {this.renderLoginField()}
          </div>
        );
    }
}
export default HeaderLoginSwitchComponent;