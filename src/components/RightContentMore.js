import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import WeeklyPressuresByBrandChart from './weeklyPressuresByBrandChart';
import EtlTable from './etlTableComponent';

class RightContentMore extends Component {

    constructor(props) {
        super(props)
        this.state = {
            fromDateLineChart: moment().subtract(3, 'months'),
            toDateLineChart: moment(),
            startDateTopicsTable: moment()
        };
        this.handleLineChartFromDateChange = this.handleLineChartFromDateChange.bind(this);
        this.handleLineChartToDateChange = this.handleLineChartToDateChange.bind(this);
    }

    handleLineChartFromDateChange(date) {
        this.state.fromDateLineChart = date;
        this.setState({
            fromDateLineChart: this.state.fromDateLineChart
        });
    }

    handleLineChartToDateChange(date) {
        this.setState({
            toDateLineChart: date
        });
    }

    handleStartDateTopicsTableChange = (date) => {
        this.state.startDateTopicsTable = date;
        this.setState({
            startDateTopicsTable: this.state.startDateTopicsTable
        });
    }

    render() {
        return (
            <div className="right-col-inner">
                <div className="inner-left">
                <div className="line-chart-container">
                        <div ><h3 className="date-label">From :</h3>
                        <DatePicker
                                selected={this.state.fromDateLineChart}
                                onChange={this.handleLineChartFromDateChange}
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                dateFormat="DD/MM/YYYY"
                            />
                            <h3 className="date-label">To :</h3>
                        <DatePicker
                                selected={this.state.toDateLineChart}
                                onChange={this.handleLineChartToDateChange}
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                dateFormat="DD/MM/YYYY"
                            />
                        </div>
                        <div className="line-chart">
                            <WeeklyPressuresByBrandChart brandId={2} fromDate={this.state.fromDateLineChart} toDate={this.state.toDateLineChart} />
                        </div>
                    </div>
                    <div className="bar-chart">
                        <EtlTable sectorId={2} date={this.state.startDateTopicsTable} />
                    </div>
                </div>
            </div>
        );
    }
}

export default RightContentMore;


/*

<table>
                            <tbody>
                                <tr>
                                    <td className="col1">Product Service Score<br /><a href="https://www.rfidjournal.com/blogs/experts/entry?12054">https://www.rfidjournal.com/blogs/experts/entry?12054</a></td>
                                    <td className="col2" bgcolor="#9ad0f5">0.285714286</td>
                                </tr>
                                <tr>
                                    <td className="col1">Innovation Score<br /><a href="https://internetofthingsagenda.techtarget.com/blog/IoT-Agenda/The-internet-of-things-without-AI-enabled-IoT-Still-dumb">https://internetofthingsagenda.techtarget.com/blog/IoT-Agenda/The-internet-of-things-without-AI-enabled-IoT-Still-dumb</a></td>
                                    <td className="col2" bgcolor="#4bc0c0">1.0</td>
                                </tr>
                                <tr>
                                    <td className="col1">Workplace Score<br /><a href="https://www.cspdailynews.com/industry-news-analysis/marketing-strategies/articles/another-shot-loyalty">https://www.cspdailynews.com/industry-news-analysis/marketing-strategies/articles/another-shot-loyalty</a></td>
                                    <td className="col2" bgcolor="#9ad0f5">0.818181818</td>
                                </tr>
                                <tr>
                                    <td className="col1">Governance Score<br /><a href="http://twitter.com/SarahL1928/statuses/1033306696460980225">http://twitter.com/SarahL1928/statuses/1033306696460980225</a></td>
                                    <td className="col2" bgcolor="#ffb0c1">-1.0</td>
                                </tr>
                                <tr>
                                    <td className="col1">Citizenship Score<br /><a href="http://twitter.com/GibbSelleck/statuses/1034580513300856832">http://twitter.com/GibbSelleck/statuses/1034580513300856832</a></td>
                                    <td className="col2" bgcolor="#ffb0c1">-0.626168224</td>
                                </tr>
                                <tr>
                                    <td className="col1">Leadership Score<br /><a href="http://thefogbow.com/forum/viewtopic.php?f=49&p=1016354">http://thefogbow.com/forum/viewtopic.php?f=49&p=1016354</a></td>
                                    <td className="col2" bgcolor="#4bc0c0">1.0</td>
                                </tr>
                                <tr>
                                    <td className="col1">Performance Score<br /><a href="https://theenterpriseleader.com/2018/08/20/bp-midstream-partners-bpmp-upgraded-to-buy-at-zacks-investment-research.html">https://theenterpriseleader.com/2018/08/20/bp-midstream-partners-bpmp-upgraded-to-buy-at-zacks-investment-research.html</a></td>
                                    <td className="col2" bgcolor="#9ad0f5">0.661538462</td>
                                </tr>
                            </tbody>
                        </table>

*/