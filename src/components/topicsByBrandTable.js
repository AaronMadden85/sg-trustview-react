import React, { Component } from 'react';
import TrustViewApi from '../api/trustViewApi';

class TopicsByBrandTable extends Component {

	constructor(props) {
		super(props);
		this.state = {
			date: props.date,
			sectorId: props.sectorId,
			topics: null
		}

		this.api = new TrustViewApi();
	}

	async componentDidMount() {
		await this.getTopics();
	}

	async getTopics() {
		const data = (await this.api.getTopics(this.state.sectorId, this.state.date.format("YYYYMMDD"))).data;

		if (data.length === 0) return;

		let table = []
		

		for(let topic of data){
			let children = []

			var color = "#ffff66";
			var sentiment = 'Neutral'
			if(topic.Sentiment === 2){
				color = "#ff0000";
				sentiment = 'Negative'
			}

			if(topic.Sentiment === 1){
				color = "#00cc66";
				sentiment = 'Positive'
			}

			children.push(<td className="col1">{topic.Description}</td>)
			children.push(<td className="col2" bgcolor={color}>{sentiment}</td>)

			table.push(<tr>{children}</tr>)
		}

		this.state.topics = table;
		this.setState(this.state);
	}

	async componentWillReceiveProps(props) {
		this.state.date = props.date;
		this.state.brandId = props.brandId;

		this.setState(this.state)

		await this.getTopics();
	}

	render() {
		const { topics } = this.state;
		if (topics === null) return <p>Loading ...</p>;

		return (
			<table>
				{this.state.topics}
			</table >
		);
	}
}

export default TopicsByBrandTable;