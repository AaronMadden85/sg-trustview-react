import React, { Component } from 'react';
import TrustViewApi from '../api/trustViewApi';
import Collapsible from 'react-collapsible';

class EtlTable extends Component {

	constructor(props) {
		super(props);
		this.state = {
			date: props.date,
			sectorId: props.sectorId,
			etl: null
		}

		this.api = new TrustViewApi();
	}

	async componentDidMount() {
		await this.getTopics();
	}

	async getTopics() {
		const data = (await this.api.getEtls(this.state.sectorId, this.state.date.format("YYYYMMDD"))).data;
		if (data.length === 0) return;
        this.state.etl = data;
        console.log(data);



		//this.state.topics = table;
		//this.setState(this.state);
	}

	async componentWillReceiveProps(props) {
		this.state.date = props.date;
		this.state.brandId = props.brandId;

		this.setState(this.state)

		await this.getTopics();
	}

	render() {
		/*const { topics } = this.state;
		if (topics === null) return <p>Loading ...</p>;
        */
		return (
            <div>
                <Collapsible trigger="Citizenship Score">
                    <p>This is the collapsible content. It can be any element or React component you like.</p>
                    <p>It can even be another Collapsible component. Check out the next section!</p>
                </Collapsible>
                <Collapsible trigger="Governance Score">
                    <p>This is the collapsible content. It can be any element or React component you like.</p>
                    <p>It can even be another Collapsible component. Check out the next section!</p>
                </Collapsible>
                <Collapsible trigger="InnovationScore">
                    <p>This is the collapsible content. It can be any element or React component you like.</p>
                    <p>It can even be another Collapsible component. Check out the next section!</p>
                </Collapsible>
                <Collapsible trigger="Leadership Score">
                    <p>This is the collapsible content. It can be any element or React component you like.</p>
                    <p>It can even be another Collapsible component. Check out the next section!</p>
                </Collapsible>
                <Collapsible trigger="Performance Score">
                    <p>This is the collapsible content. It can be any element or React component you like.</p>
                    <p>It can even be another Collapsible component. Check out the next section!</p>
                </Collapsible>
                <Collapsible trigger="Product Service Score">
                    <p>This is the collapsible content. It can be any element or React component you like.</p>
                    <p>It can even be another Collapsible component. Check out the next section!</p>
                </Collapsible>
                <Collapsible trigger="Workplace Score">
                    <p>This is the collapsible content. It can be any element or React component you like.</p>
                    <p>It can even be another Collapsible component. Check out the next section!</p>
                </Collapsible>
            </div>
		);
	}
}

export default EtlTable;