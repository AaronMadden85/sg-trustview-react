import React, { Component } from 'react';
import WeeklyPressuresBySectorChart from './weeklyPressuresBySectorChart';
import WeeklyPressuresByBrandChart from './weeklyPressuresByBrandChart';
import TopicsByBrandTable from './topicsByBrandTable';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

class RightContentMain extends Component {

    constructor(props) {
        super(props)
        this.state = {
            startDateScatterPlot: moment(),
            fromDateLineChart: moment().subtract(3, 'months'),
            toDateLineChart: moment(),
            startDateTopicsTable: moment(),
        };
        this.handleScatterDateChange = this.handleScatterDateChange.bind(this);
        this.handleLineChartFromDateChange = this.handleLineChartFromDateChange.bind(this);
        this.handleLineChartToDateChange = this.handleLineChartToDateChange.bind(this);
    }

    handleScatterDateChange(date) {
        this.setState({
            startDateScatterPlot: date
        });
    }

    handleLineChartFromDateChange(date) {
        this.state.fromDateLineChart = date;
        this.setState({
            fromDateLineChart: this.state.fromDateLineChart
        });
    }

    handleLineChartToDateChange(date) {
        this.setState({
            toDateLineChart: date
        });
    }

    handleStartDateTopicsTableChange = (date) => {
        this.state.startDateTopicsTable = date;
        this.setState({
            startDateTopicsTable: this.state.startDateTopicsTable
        });
    }

    render() {
        return (
            <div className="right-col-inner">
                <div className="inner-left">
                    <div className="line-chart-container">
                        <div ><h3 class="date-label">From :</h3>
                        <DatePicker
                                selected={this.state.fromDateLineChart}
                                onChange={this.handleLineChartFromDateChange}
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                dateFormat="DD/MM/YYYY"
                            />
                            <h3 class="date-label">To :</h3>
                        <DatePicker
                                selected={this.state.toDateLineChart}
                                onChange={this.handleLineChartToDateChange}
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                dateFormat="DD/MM/YYYY"
                            />
                        </div>
                        <div className="line-chart">
                            <WeeklyPressuresByBrandChart brandId={2} fromDate={this.state.fromDateLineChart} toDate={this.state.toDateLineChart} />
                        </div>
                    </div>
                    <div >
                        <h3 class="date-label">Date :</h3>
                        <DatePicker
                            selected={this.state.startDateScatterPlot}
                            onChange={this.handleScatterDateChange}
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            dateFormat="DD/MM/YYYY"
                        />
                    </div>

                    <div className="bubble-chart">
                        <div className="bubble-container">
                            <WeeklyPressuresBySectorChart sectorId={1} date={this.state.startDateScatterPlot} />
                        </div>
                    </div>
                    <div className="date-picker-container">
                    <h3 className="date-label">Date :</h3>
                    <DatePicker
                                selected={this.state.startDateTopicsTable}
                                onChange={this.handleStartDateTopicsTableChange}
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                dateFormat="DD/MM/YYYY"
                                placeholderText="CLICK"
                            />
                            </div>
                    <div className="bar-chart">
                        <div>
                            <TopicsByBrandTable sectorId={2} date={this.state.startDateTopicsTable} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default RightContentMain;