import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import TrustViewApi from '../api/trustViewApi';

class WeeklyPressuresByBrandChart extends Component {

	constructor(props) {
		super(props);
		this.state = {
			fromDate: props.fromDate,
			toDate: props.toDate,
			brandId: props.brandId,
			weeklyPressures: null
		}

		this.api = new TrustViewApi();
	}

	async componentDidMount() {
		await this.getWeeklyPressures();
	}

	async getWeeklyPressures() {
		const data = (await this.api.getWeeklyPressuresByBrand(this.state.brandId, this.state.fromDate.format("YYYYMMDD"), this.state.toDate.format("YYYYMMDD"))).data;

		var weeklyPressures = { labels: [], datasets: [] };

		if (data.StandardDeviation === undefined) return;

		//std dev
		weeklyPressures = {
			labels: [],
			datasets: [
				{
					label: 'Pressure Score',
					fill: false,
					backgroundColor: '#0066cc',
					borderColor: '#0066cc',
					radius: 0,
					data: [],
				},
				{
					label: '-1 Std',
					fill: false,
					backgroundColor: '#ff0000',
					borderColor: '#ff0000',
					radius: 0,
					data: [],
				}, {
					label: 'Mean',
					fill: false,
					backgroundColor: '#ff8000',
					borderColor: '#ff8000',
					radius: 0,
					data: [],
				}, {
					label: '+1 Std',
					fill: false,
					backgroundColor: '#339933',
					borderColor: '#339933',
					radius: 0,
					data: [],
				}]
		};

		for (let index in data.WeeklyPressures) {
			var stringDate = data.WeeklyPressures[index].WeekEndDate;

			weeklyPressures.labels.push(stringDate.substring(0, 6) + stringDate.substring(8, 10));

			weeklyPressures.datasets[0].data.push(data.WeeklyPressures[index].PressureScore)
			weeklyPressures.datasets[1].data.push(data.StandardDeviation.Minus1Std)
			weeklyPressures.datasets[2].data.push(data.StandardDeviation.Mean)
			weeklyPressures.datasets[3].data.push(data.StandardDeviation.Plus1Std)
		};

		this.state.weeklyPressures = weeklyPressures;
		this.setState(this.state)
	}

	async componentWillReceiveProps(props) {
		this.state.fromDate = props.fromDate;
		this.state.toDate = props.toDate;
		this.state.brandId = props.brandId;

		this.setState(this.state)

		await this.getWeeklyPressures();
	}

	render() {
		const { weeklyPressures } = this.state;
		if (weeklyPressures === null) return <p>Loading ...</p>;

		return (
			<Line data={weeklyPressures} options={{
				scales: {
					yAxes: [
						{
							ticks: {
								callback: function (label, index, labels) {
									return label * 100 + '%';
								}
							},
							scaleLabel: {
								display: true
							}
						}
					]
				},
				legend: {
					display: false
				},
				tooltips: {
					"enabled": false
				},
				maintainAspectRatio: false
			}} />
		);
	}
}

export default WeeklyPressuresByBrandChart;