import React, { Component } from 'react';
import TrustViewApi from '../api/trustViewApi';
import ClientInfoTable from './clientInfoTable';

class NavLeftColComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      client: null
    }
		this.api = new TrustViewApi();
	}

	async componentDidMount() {
		await this.getClients();
	}

	async getClients() {
		const data = (await this.api.getClientsBySector(1)).data;

		this.state.client = data[3].Item;
		this.setState(this.state);
	}

  render() {
    const { client } = this.state;
		if (client === null) return <p>Loading ...</p>;
    return (
      <ClientInfoTable client={client} />
    );
  }
}
export default NavLeftColComponent;
























