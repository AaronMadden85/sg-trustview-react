import React, { Component } from 'react';
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Main from "./RightContentMain";
import More from "./RightContentMore";

class NavRightColComponent extends Component {

render() {
    return (
        <HashRouter>
            <div className="right-col">
                <div className="right-content">
                    <Route exact path="/" render={(props) => <Main {...props} routeProps={this.props.propsData} />}/>
                    <Route path="/more" render={(props) => <More {...props} routeProps={this.props.propsData} />}/>
                </div>
                <ul className="right-nav">
                    <li><NavLink exact to="/">BACK TO OVERVIEW</NavLink></li>
                    <li><NavLink to="/more">MORE DETAIL</NavLink></li>
                </ul>
            </div>
        </HashRouter>
    );
  }
}
export default NavRightColComponent;