import React, { Component } from 'react';

class ClientInfoTable extends Component {

	constructor(props) {
		super(props);
		this.state = {
			client: props.client
		}
	}

	async componentWillReceiveProps(props) {
		this.state.client = props.client;
		this.setState(this.state)
	}

	render() {
		const { client } = this.state;
		if (client === null) return <p>Loading ...</p>;

		return (
			<div className="left-col">
				<h1>Company Information</h1>
				<div className="abcContainer">
					<img src={client.Logo} alt="abc logo" className="abc" />
					<h2>{client["Legal Name"]}</h2>
				</div>
				<div className="divTable">
					<div className="divTableBody">
						<div className="divTableRow">
							<div className="divTableCell"><h3>Level :</h3></div>
							<div className="divTableCell"><h4>GBR</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Ccial BDR ID :</h3></div>
							<div className="divTableCell"><h4>GRP</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Legal BDR ID :</h3></div>
							<div className="divTableCell"><h4>101</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>RCT ID :</h3></div>
							<div className="divTableCell"><h4>63678</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Segmentation :</h3></div>
							<div className="divTableCell"><h4>123</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Steering Ctry :</h3></div>
							<div className="divTableCell"><h4>STRATEGIC CORP</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Typology :</h3></div>
							<div className="divTableCell"><h4>EURO-UK-o/w UK</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>PCRU :</h3></div>
							<div className="divTableCell"><h4>Commodities</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Parent Group :</h3></div>
							<div className="divTableCell"><h4>CORI/COV/EUR/GBR/OIL</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Address :</h3></div>
							<div className="divTableCell"><h4>{client.Address}</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Banker :</h3></div>
							<div className="divTableCell"><h4>{client.RM}</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>RM :</h3></div>
							<div className="divTableCell"><h4>{client.RM}</h4></div>
						</div>
						<div className="divTableRow">
							<div className="divTableCell"><h3>Local RM :</h3></div>
							<div className="divTableCell"><h4>{client["Local RM"]}</h4></div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ClientInfoTable;