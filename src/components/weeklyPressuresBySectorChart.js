import React, { Component } from 'react';
import { Bubble } from 'react-chartjs-2';
import TrustViewApi from '../api/trustViewApi';

class WeeklyPressuresBySectorChart extends Component {

	constructor(props) {
		super(props);
		this.state = {
			date: props.date,
			sectorId: props.sectorId,
			weeklyPressures: null,
			min: 10000000,
			max: -1
		}

		this.api = new TrustViewApi();
	}

	async componentDidMount() {
		await this.getWeeklyPressures();
	}

	dynamicColorsByIndex(index) {
		var colors = ['#9370DB', 'yellow', 'orange', 'green', 'red']
		var r = Math.floor(Math.random() * 255);
		var g = Math.floor(Math.random() * 255);
		var b = Math.floor(Math.random() * 255);
		return colors[index % colors.length]
	};

	async getWeeklyPressures() {
		const data = (await this.api.getWeeklyPressuresBySector(this.state.sectorId, this.state.date.format("YYYYMMDD"))).data;

		if (data.length === 0) return;

		var weeklyPressures = { datasets: [] };

		var min = 10000000;
		var max = -1;
		for (let index in data) {
			var currentVolume = data[index].VolumeNumber;

			if (currentVolume > max) {
				max = currentVolume;
			}

			if (currentVolume < min) {
				min = currentVolume;
			}
			weeklyPressures.datasets.push({
				label: [data[index].BrandName],
				data: [
					{ x: data[index].PressureScore, y: data[index].VolumeNumber, r: 5 },
				],
				backgroundColor: [this.dynamicColorsByIndex(index)]
			});
		};

		this.state.weeklyPressures = weeklyPressures;
		this.state.min = min;
		this.state.max = max;

		this.setState(this.state);
	}

	async componentWillReceiveProps(props) {
		this.state.date = props.date;
		this.state.sectorId = props.sectorId;

		this.setState(this.state)

		await this.getWeeklyPressures();
	}

	render() {
		const { weeklyPressures } = this.state;
		if (weeklyPressures === null) return <p>Loading ...</p>;

		return (
			<Bubble data={weeklyPressures} options={{
				tooltips: {
					"enabled": false
				},
				maintainAspectRatio: false,
				scales: {
					yAxes: [{
						ticks: {
							display: false,
							min: (this.state.min - (this.state.max - this.state.min) * 0.1),
							max: (this.state.max + (this.state.max - this.state.min) * 0.1),
							stepSize: (this.state.max - this.state.min) * 1.2 / 2
						},
						gridLines: {
							display: true
						},
						scaleLabel: {
							display: true,
							labelString: 'Number of Conversations'
						}
					}],
					xAxes: [{
						ticks: {
							display: false,
							min: -1,
							max: 1,
							stepSize: 1
						},
						scaleLabel: {
							display: true,
							labelString: 'Pressure Score'
						}
					}]
				},
			}} />
		);
	}
}

export default WeeklyPressuresBySectorChart;