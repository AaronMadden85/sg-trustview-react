import React, { Component } from 'react';
import HeaderLoginSwitchComponent from './HeaderLoginSwitchComponent'

class NavHeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.handleAuth = this.handleAuth.bind(this);
    }
    handleAuth() {
        this.props.onAuth();
    }
    render() {
        return (
            <header className="DashboardContainer-header">
                <img className="logo-img" src="./img/logo.png"  alt="Logo"/>
                <div className="login-container">
                    <HeaderLoginSwitchComponent onAuth={this.handleAuth}/>
                </div>
            </header>
        );
    }
}
export default NavHeaderComponent;